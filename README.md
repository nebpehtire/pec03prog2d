
# Pec03Prog2D

PEC 03 de la asignatura Programación 2D
Máster de Diseño y Programación de Videojuegos.

## Authors

- [@Nebpehtire](https://github.com/nebpehtire) Emanuel Xavier Sitaras Stier

  
## Documentation

A) Scenes
1) Menu: Escena principal del juego donde se puede seleccionar:
- Iniciar el juego
- Salir del juego

2) Level2-PEC03
- Se ejecuta la lógica del juego



### Proceso de juego 



1) Modo Normal:
El juego inicia a Mario en el centro de la pantalla
Se inicia con cinco vidas
Movimiento:
flecha izquierda y tecla A para mover hacia la izquierda
flecha derecha y tecla D para mover hacia la derecha
espacio y tecla W para saltar
Shift izquierdo para aumentar la velocidad de Mario
click izq ratón para lanzar bola energía de Mario
tecla esc para pausar el juego

## Demo


Youtube: [Video presentación](https://youtu.be/ufitt60wcAY)


## Invokes && Coroutines
Podemos encontrar uso de Invoke en la clase GameManager, WinnedPlay, realiza un Invoke hacia la habilitación del panel de juego terminado.
Podemos encontrar uso de Coroutine en MarioManager, Startcoroutine en IsDead() => Coroutine IEnumerator RespawnOrDied().


## Clases y cambios de PEC03


**FolowMario(cambios):** 
- Se habilita el movimiento vertical de la cámara, se añade un seguimiento fluido a mario.


**MarioMovement(cambios):**
* Update: se añade lanzamiento de bolas de energía con el click izq. del ratón.
* Shot: crea una nueva instancia de bullet y lo lanza dependiendo de hacia donde esté mirando (izq/der)
* CheckDownRays: se arreglan issues en esta función

**Bullet:** Clase para las bolas de energía de mario
* OnCollisionEnter2D: si es un tag == floor desaparece con un prefab de partículas, si es un enemigo desaparece con otro.
(los bullets solo empujan los enemigos con su rigidbody)

**Coin:** clase para las monedas esparcidas en la pantalla
* OnTriggerEnter2D: si es mario => suma 1 en el score de coins, inicia una animación y sonido de moneda.

**Enemy:**
- se añade un nuevo enemigo llamado FinalBoss
* Death: si el enemigo es un FB, habilita el final de la pantalla.

**EnemyMovement:**
Se cambia GoombaMoovent a EnemyMoovement
Se habilita sensor para no caer de una plataforma analizando si lo que tiene delante es un hueco o suelo, cambiando el sentido de la marcha si es lo primero.

**FinalBossShoot:**
Después de un tiempo Instancia un nuevo FinalBossBullet hacia donde esté mirando.

**IAMovementGoomba:** Controlador de sentido de movimiento de los enemigos
* enemyMovement: carga la case que controla el movimiento
* target: referenciará  al enemigo (mario) cuando esté cerca 
* OnTriggerEnter2D: si es mario lo referencia al target
* OnTriggerExit2D: si es mario lo desvincula del target
* Update: si el target no es null calcula el cross entre los vectores posición para saber hacia donde ir


**BulletFinalBoss:** Clase para los objetos bullet del FB
* OnCollisionEnter2D: si es mario le quita vida, si no es desaparece, en ambos casos crea una explosión mediante un prefab de partículas.

**ActivatorLift:** Activa un ascensor si un enemigo es eliminado (ej: usado en el gusano)

**FinalTrigger:**
Finaliza el juego con victoria, lanzando música y animación final de Mario.

**MusicManager(cambios):** Se añaden nuevos sonidos al enum y dictionary
* WinnedLevel: para la música general y suenan el level complete y grito de alegría de mario





## Clases de PEC02


**MenuManager:** Controlador para el menú principal
* BTN_PlayGame: carga el nivel del juego
* BTN_QuitGame: Cierra la aplicaicón




**MusicManager:** Controlador de la música y sonidos del juego

*Funciones*
* AudioDictionary: diccionario con todos los sonidos
* CreateDictionary: crea el diccionario de carga de sonidos
* StopMusic: Detiene la reproducción
* PauseMusic: Pausa la reproducción
* StartMusic: Inicia la música desde cero
* PlayOnShotSound: inicia un clip de audioDictionary
* PlayClick: sonido click básico





**GameManager:** Lógica general del juego.

*Funciones más relevantes*

* TimerOn: Conteo del timer
* SumDestroyEnemy: Suma puntos y los visualiza al matar un enemigo
* SumAcoin: Suma y visualiza al conseguir una moneda
* SumAPowerUp: Suma y visualiza los puntos al conseguir un powerUP
* BTN_PlayGame: sale de la pantalla pausa
* RespawnMario: Respawn de mario en el último respawn
* MariosHasEnteredInARespawn: se guardan los datos de respawn

**GoombaMovement:**  Controla el movimiento de los goomba

*Funciones más relevantes*
* Walk
* CheckTouchingFloor: Chequea si toca el suelo, devuelve true/false
* Fall: Cae el goomba, cambiamos a estado falling
* CheckWalls: chequeamos si tocamos una pared

**MarioManager:** Clase de Mario
*Funciones más relevantes*
* IsDead: muerte de mario
* ReceiveDamage: recibe daño
* RespawnOrDied: Temporizador para lanzar función de gamemanager y destruir este mario


**MarioMovement:** Controla el movimiento de Mario

*Funciones más relevantes*
* Jump: realiza un salto
* JumpWithEnemyDestroy: salto al caer en un enemigo
* TouchingGround: devuelve si estamos encima de un elemento
* CheckDownRays: Chequea qué va a encontrar al caer

**MysteryBox:** Clase para las cajas misteriosas
* SumAndSound: inicia animación, suma coin e inicia sonido

**ScoreManager:** Manager para las puntuaciones
**SimpleBlock:** Clase para los bloques simples

## Referencias de contenido usado.
Prefabs de partículas (Se ha hecho limpieza de prefabs de partículas no usadas, pero no de textures por si acaso crasheo alguna)
Cartoon FX Free de Jean Moreno
https://assetstore.unity.com/packages/vfx/particles/cartoon-fx-free-109565

Sprites SMB 
https://www.spriters-resource.com/fullview/33489/

players
https://www.spriters-resource.com/fullview/34229/

Enemies
https://www.spriters-resource.com/fullview/22919/

Music
http://www.sonidosmp3gratis.com/

music 2:
https://downloads.khinsider.com/game-soundtracks/album/super-mario-bros

music 3:
https://www.sounds-resource.com/game_boy_advance/marioluigisuperstarsaga


  