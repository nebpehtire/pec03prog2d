using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using MSSUOC.Mario.Entities;

namespace MSSUOC.Mario.Shoot
{

    public class BulletFinalBoss : MonoBehaviour
    {

        public GameObject fxBulletFinalBoss;

        /// <summary>
        /// Si colisiona con el jugador le produce daño, si no es el jugador desaparece con un efecto
        /// </summary>
        /// 
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.transform.tag != "Player")
            {
                var newFx = Instantiate(fxBulletFinalBoss, transform.position, transform.rotation);
                Destroy(gameObject);
            }
            else
            {
                collision.gameObject.GetComponent<MarioManager>().ReceiveDamage();
                var newFx = Instantiate(fxBulletFinalBoss, collision.transform.position, transform.rotation);
                Destroy(gameObject);
            }


        }

    }
}