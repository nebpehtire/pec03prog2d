using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using MSSUOC.Mario.Manager;


/// <summary>
/// Clase para las monedas desperdigadas
/// </summary>
public class Coin : MonoBehaviour
{
    bool active = true;
    Animator animator;
    private void Awake()
    {
        animator = GetComponentInChildren<Animator>();
    }


    /// <summary>
    /// Si Mario entra la moneda se suma, suena sonido y activa invoke para destruirla
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && active)
        {
            active = false;
            animator.SetBool("coinWinned", true);
            FindObjectOfType<GameManager>().SumAcoin();
            FindObjectOfType<MusicManager>().PlayOnShotSound(MarioSound.Coin);

            Invoke(nameof(DestroyMe),1.5f);

        }
    }
    private void DestroyMe()
    {
        Destroy(gameObject);
    }
}
