using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MSSUOC.Mario.Entities
{

    public class SimpleBlock : MonoBehaviour
    {


        /// <summary>
        /// Si mario toca el bloque, se mueve
        /// </summary>
        /// <param name="collision"></param>
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "Player")
            {
                GetComponentInParent<Animator>().SetBool("miniMarioTouchDown", true);
            }

        }
    }

}
