using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MSSUOC.Mario.Entities
{
    public enum EnemyClass
    {
        Koopa,
        Goomba,
        PiranhaPlant,
        Oruga,
        FinalBoss
    }

    public class Enemy : MonoBehaviour
    {

        public enum EnemyState
        {
            waiting,
            falling,
            walking,
            withDamage,
            dead
        }


        public EnemyClass enemyClass;
        public bool isDeath = false;
        public Animator animator;

        public bool canWalk = true;
        public bool isWalkingLeft = false;

        public Vector3 initialPosition;

        public GameObject FXDeath;


        public int life;
        private int restoreLife;
        [SerializeField] public EnemyState state;


        private bool shouldDie = false;
        private float timerToDie = 0.0f;
        public float timeBeforeDetroy = 1.0f;


        private void Awake()
        {
            animator = GetComponent<Animator>();
            restoreLife = life;

            

        }


      

        //private void Start()
        //{
        //    initialPosition = gameObject.transform.position;
        //    if (life == 0)
        //        Debug.Log("life es cero");
        //}


        private void Death()
        {
            state = EnemyState.dead;

            animator.SetBool("isDead", true);

            shouldDie = true;

            isDeath = true;

            FindObjectOfType<Manager.GameManager>().SumDestroyEnemy(enemyClass);

            GameObject fx = Instantiate(FXDeath);
            fx.transform.position = transform.position;

            if (enemyClass == EnemyClass.FinalBoss)
            {
                FindObjectOfType<Manager.GameManager>().EnableFinalGO();

                FindObjectOfType<Manager.MusicManager>().PlayOnShotSound(Manager.MarioSound.FinalBossDead);
            }
                

        }





        /// <summary>
        /// Inicia animación de enemigo muerto
        /// suma puntos al score
        /// </summary>
        public bool MarioCauseDamage()
        {
            if (state != EnemyState.withDamage)
            {
                state = EnemyState.withDamage;
                life--;
                if (life <= 0)
                    Death();
                else
                {
                    
                    animator.SetBool("receivedDamage", true);
                    //Invoke(nameof(ReturnToWalking), 2f);
                }
                return true;  
            }



            return false;


        }

        private void ReturnToWalking()
        {
            state = EnemyState.walking;
        }

        /// <summary>
        /// Chequea si está muerto
        /// </summary>
        public void CheckDied()
        {
            if (shouldDie)
                if (timerToDie <= timeBeforeDetroy)
                    timerToDie += Time.deltaTime;
                else
                {
                    shouldDie = false;
                    Destroy(this.gameObject);
                }
        }

        /// <summary>
        /// Restaura a la posición inicial
        /// </summary>
        public void RestorePosition()
        {
            this.transform.position = initialPosition;
            GetComponent<CapsuleCollider2D>().enabled = true;
            state = EnemyState.walking;
            isDeath = false;
            life = restoreLife;
        }



    }

}
