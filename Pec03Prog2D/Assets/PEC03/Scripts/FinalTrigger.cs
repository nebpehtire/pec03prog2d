using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace MSSUOC.Mario.Entities
{

    /// <summary>
    /// Trigger para finalizar el level
    /// </summary>
    public class FinalTrigger : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "Player")
                FindObjectOfType<Manager.GameManager>().WinnedPlay();
        }
    }

}

