using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MSSUOC.Mario.Entities;

namespace MSSUOC.Mario.Movement
{


    public class EnemyMovement : Enemy
    {

        public float speed;
        
        private Vector2 velocity;
        public LayerMask floorMask;
        public LayerMask wallMask;
        public bool canFall = false;


        private Vector3 diference = new Vector3(0.5f, 0, 0);

        private Manager.GameManager gameManager;

        private void Awake()
            
        {
            if (life == 0)
                life = 1;

            gameManager = FindObjectOfType<Manager.GameManager>();

            if (speed <= 0.0f)
                speed = 1.0f;

            state = EnemyState.waiting;

            


        }
        // Start is called before the first frame update
        void Start()
        {
            initialPosition = gameObject.transform.position;

            enabled = false;
        }

        // Update is called once per frame
        void Update()
        {
            if (gameManager.stateGame == Manager.StateGame.Playing)
                UpdatePosition();
        }


        /// <summary>
        /// Modifica la posición del goomba dependiendo de en qué estado está
        /// </summary>
        private void UpdatePosition()
        {
            
            switch (state)
            {
                case EnemyState.waiting:
                    break;
                case EnemyState.falling:
                    Falling();
                    
                    break;
                case EnemyState.walking:
                    Walk();
                    break;
                case EnemyState.dead:
                    break;
            }




        }


        /// <summary>
        /// Chequea si toca el suelo, si es así inicia walking
        /// </summary>
        private void Falling()
        {
            if (CheckTouchingFloor())
                state = EnemyState.walking;
            

        }

        /// <summary>
        /// Movimiento del Goomba
        /// </summary>
        private void Walk()
        {
            Vector3 pos = transform.localPosition;
            Vector3 scale = transform.localScale;


            if (isWalkingLeft)
            {
                transform.Translate(new Vector3(-speed, 0, 0) * Time.deltaTime);
                scale.x = -1;
            }
            else
            {
                transform.Translate(new Vector3(speed, 0, 0) * Time.deltaTime);
                scale.x = 1;
            }

            if (velocity.y <= 0)
                pos = CheckFloor(pos);


            CheckWalls(scale.x);

            transform.localScale = scale;

            if (!CheckTouchingFloor())
            {
                isWalkingLeft = !isWalkingLeft;
            }
                


        }

        /// <summary>
        /// Chequea si toca el suelo, devuelve true/false
        /// </summary>
        /// <returns></returns>
        private bool CheckTouchingFloor()
        {
            Vector3 rayPositionLeft = transform.position - diference;
            Vector3 rayPositionRight = transform.position + diference;
            Debug.DrawRay(transform.position, Vector3.down, Color.red);
            return Physics2D.Raycast(rayPositionLeft, Vector3.down, 2f, floorMask) && Physics2D.Raycast(rayPositionRight, Vector3.down, 2f, floorMask);

        }


        /// <summary>
        /// Devuelve vector de posición si toca el suelo
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        ///
        ///TODO: revisar estas dos funciones
        private Vector3 CheckFloor(Vector3 pos)
        {
            Vector2 originLeft = new Vector2(pos.x - 0.5f, pos.y);
            Vector2 originMiddle = new Vector2(pos.x, pos.y);
            Vector2 originRight = new Vector2(pos.x + 0.5f, pos.y);

            float distance = 1f;
            if (this.enemyClass == EnemyClass.FinalBoss)
                distance = 2f;

            RaycastHit2D groundLeft = Physics2D.Raycast(originLeft, Vector2.down, distance, floorMask);
            RaycastHit2D groundMiddle = Physics2D.Raycast(originMiddle, Vector2.down, distance, floorMask);
            RaycastHit2D groundRight = Physics2D.Raycast(originRight, Vector2.down, distance, floorMask);

            if (groundLeft.collider != null || groundMiddle.collider != null || groundRight.collider != null)
            {
                RaycastHit2D hitRay = groundLeft;

                if (groundLeft)
                    hitRay = groundLeft;
                else if (groundMiddle)
                    hitRay = groundMiddle;
                else if (groundRight)
                    hitRay = groundRight;


                pos.y = hitRay.collider.bounds.center.y + hitRay.collider.bounds.size.y / 2 + 0.5f;

                velocity.y = 0;
                state = EnemyState.walking;
            }
            else 
            {
                if (canFall)
                {
                    if (state != EnemyState.falling)
                    {
                        Fall();
                    }
                }
                else
                {
                    isWalkingLeft = !isWalkingLeft;
                }
                
            }

            return pos;


        }

        /// <summary>
        /// Cae el goomba, cambiamos a estado falling
        /// </summary>
        private void Fall()
        {
            velocity.y = 0;
            state = EnemyState.falling;
        }

        /// <summary>
        /// Chequeamos si tocamos una pared
        /// si es así cambiamos el sentido de la marcha
        /// </summary>
        /// <param name="direction"></param>
        private void CheckWalls(float direction)
        {

            float force = 0.6f;
            if (enemyClass == EnemyClass.Oruga || enemyClass == EnemyClass.FinalBoss)
                force += 1f;
            Debug.DrawRay(transform.position, new Vector3(direction, 0, 0) * force, Color.red);

            RaycastHit2D hit = Physics2D.Raycast(transform.position, new Vector3(direction, 0, 0) * force, force);

            if (hit.collider != null)
                if (hit.collider.tag == "Walls" || hit.collider.tag == "Floor" )
                    isWalkingLeft = !isWalkingLeft;

        }

        /// <summary>
        /// Cuando es visible por una cámara se activa el walking
        /// </summary>
        private void OnBecameVisible()
        {

            state = EnemyState.walking;
            enabled = true;
        }


        /// <summary>
        /// Cuando no es visible por una cámara cambia a waiting (esencial en los respawns)
        /// </summary>
        private void OnBecameInvisible()
        {
            state = EnemyState.waiting;
        }

        /// <summary>
        /// Si toca al player le provoca daño
        /// </summary>
        /// <param name="collision"></param>
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.collider.tag == "Player" && !isDeath)
            {
                collision.collider.GetComponent<MarioManager>().ReceiveDamage();
            }

        }

    }

}