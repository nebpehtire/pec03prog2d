using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using MSSUOC.Mario.Entities;

namespace MSSUOC.Mario.Manager
{
    public enum StateGame
    {
        InitialPanel,
        Playing,
        Pause,
        GameOver,
        WinnedGame
    }
    
    public class GameManager : MonoBehaviour
    {



        [SerializeField] private ScoreManager scoreManager;

        public int timeGame;
        [SerializeField] private int timeLeftGame;
        [SerializeField] private int lifesOfMario = 3;
        private float toCurrentSecond;


        [SerializeField] private int points;
        [SerializeField] private int coins;
        public StateGame stateGame;


        public Entities.MarioManager marioPlayer;
        private Entities.MarioManager mario;

        public Movement.FollowMario followMario;

        public Vector3 respawnPosition;
        public bool hasPassedRespawn = false;
        private int respawnCoins;
        private int respawnScore;
        private int respawnTime = 400;


        [Space]
        public GameObject initialPanel;
        public GameObject ResetGamePanel;


        private StateGame pivotSateGame;

        public Transform enemiesTransformParent;

        private Dictionary<EnemyClass, int> enemyPoints = new Dictionary<EnemyClass, int>();


        public GameObject finalGO;

        private void Awake()
        {

            Application.targetFrameRate = 60;

            if (timeGame == 0)
                timeGame = 400;
            timeLeftGame = timeGame;
            toCurrentSecond = timeLeftGame + 1;

            if (lifesOfMario <= 0)
                lifesOfMario = 3;




            stateGame = StateGame.Playing;

            points = 0;
            coins = 0;


            scoreManager = FindObjectOfType<ScoreManager>();

            mario = Instantiate(marioPlayer);
            mario.transform.position = new Vector2(-2.0f, 0f);
            followMario.marioGO = mario;


            FullEnemyDictionaryPoints();

        }

        // Start is called before the first frame update
        void Start()
        {

            FindObjectOfType<MusicManager>().StartMusic();
            scoreManager.AllScoresByValor(points, coins, timeGame);
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
               {
                if (stateGame != StateGame.Pause && stateGame != StateGame.GameOver && stateGame != StateGame.WinnedGame)
                {
                    pivotSateGame = stateGame;
                    stateGame = StateGame.Pause;
                    initialPanel.SetActive(true);
                    mario.GetComponent<Movement.MarioMovement>().enabled = false;
                    mario.GetComponent<Rigidbody2D>().Sleep();
                    FindObjectOfType<MusicManager>().PauseMusic(); ;
                }

                else
                {
                    this.BTN_PlayGame();
                    stateGame = pivotSateGame;
                }
                    
                }
                



            if (stateGame == StateGame.Playing)
            {
                TimerOn();
            }

            if (timeLeftGame <= 0 && stateGame != StateGame.GameOver)
            {
                stateGame = StateGame.GameOver;
                lifesOfMario = 0;
            }
                

            if (stateGame == StateGame.GameOver && lifesOfMario == 0)
                ShowPanelGameOver();

            
        }

        /// <summary>
        /// Conteo del timer, se realiza en modo rápido como el juego original
        /// </summary>
        private void TimerOn()
        {
          

            if (toCurrentSecond <= timeLeftGame)
            {
                timeLeftGame--;
                scoreManager.TimeByInt(timeLeftGame);
            }
            else
            {
                toCurrentSecond -= Time.deltaTime * 4;
   
            }
        }


        /// <summary>
        /// Suma puntos y los visualiza al matar un enemigo
        /// </summary>
        /// <param name="sumPoints"></param>
        public void SumDestroyEnemy(EnemyClass enemyClass)
        {
            int sumPoints = enemyPoints[enemyClass];
            points += sumPoints;
            scoreManager.ScoreByInt(points);
        }

        

        private void FullEnemyDictionaryPoints()
        {
            enemyPoints.Add(EnemyClass.Koopa, 100);
            enemyPoints.Add(EnemyClass.Goomba, 100);
            enemyPoints.Add(EnemyClass.PiranhaPlant, 300);
            enemyPoints.Add(EnemyClass.Oruga, 500);
            enemyPoints.Add(EnemyClass.FinalBoss, 1000);
        }

        /// <summary>
        /// Suma y visualiza al conseguir una moneda
        /// </summary>
        public void SumAcoin()
        {
            coins++;
            scoreManager.CoinsByInt(coins);
            points += 200;
            scoreManager.ScoreByInt(points);
        }

        /// <summary>
        /// Suma y visualiza los puntos al conseguir un powerUP
        /// </summary>
        public void SumAPowerUp()
        {
            points += 1000;
            scoreManager.ScoreByInt(points);
        }


        /// <summary>
        /// Ejecuta el juego
        /// </summary>
        public void BTN_PlayGame()
        {
           

            stateGame = StateGame.Playing;
            initialPanel.SetActive(false);
            mario.GetComponent<Movement.MarioMovement>().enabled = true;
            FindObjectOfType<MusicManager>().PlayMusic();
        }

        /// <summary>
        /// Carga la escena Menu
        /// </summary>
        public void BTN_ReturnToMenu()
        {
            SceneManager.LoadScene("Menu");
        }

        /// <summary>
        /// Vuelve a cargar el nivel 1
        /// </summary>
        public void BTN_PlayAgain()
        {
            SceneManager.LoadScene("Level2-PEC03");
        }

        /// <summary>
        /// Respawn de mario en el último respawn
        /// Si no ha pasado por ninguno realiza el respawn en la posición inicial (-2,0)
        /// </summary>
        private void RespawnMario()
        {
            ResetAllEnemies();
            ResetTimeScoreCoins();

            mario = Instantiate(marioPlayer);
            if (hasPassedRespawn)
                mario.transform.position = respawnPosition;
            else
                mario.transform.position = new Vector2(-2, 0);

            followMario.marioGO = mario;
            followMario.transform.position = new Vector3(mario.transform.position.x, followMario.transform.position.y, followMario.transform.position.z);
            FindObjectOfType<MusicManager>().PlayMusic();
            stateGame = StateGame.Playing;

        }

        /// <summary>
        /// Muestra el panel de juego acabado
        /// </summary>
        private void ShowPanelGameOver()
        {
            ResetGamePanel.SetActive(true);
        }

        /// <summary>
        /// Resetea el score manager con los datos del último respawn
        /// </summary>
        private void ResetTimeScoreCoins()
        {
            points = respawnScore;
            timeLeftGame = respawnTime;
            coins = respawnCoins;

            scoreManager.AllScoresByValor(points, coins, timeGame);
        }

        /// <summary>
        /// Resetea la posición de los enemigos
        /// </summary>
        private void ResetAllEnemies()
        {
            foreach (Transform tr in enemiesTransformParent)
            {
                Entities.Enemy enemy = tr.GetComponent<Entities.Enemy>();
                enemy.RestorePosition();
            }
        }

        /// <summary>
        /// Mario ha perdido una vida, dependiendo de cuántas queden
        /// se realiza un gameover o un respawn
        /// </summary>
        public void MarioHasLosedOneLife()
        {
            
            lifesOfMario--;
            if (lifesOfMario == 0)
                ShowPanelGameOver();
            else
                RespawnMario();
        }

        /// <summary>
        /// Mario ha entrado en un respawn
        /// Se guardan los datos de la partida
        /// </summary>
        /// <param name="position"></param>
        public void MariosHasEnteredInARespawn(Vector3 position)
        {
            this.respawnPosition = position;
            hasPassedRespawn = true;
            respawnScore = points;
            respawnCoins = coins;
            respawnTime = timeLeftGame;

        }

        /// <summary>
        /// Se ha ganado el juego
        /// Se carga el mismo panel que el game over pues no hay nivel 2
        /// </summary>
        public void WinnedPlay()
        {

            stateGame = StateGame.WinnedGame;

            FindObjectOfType<MusicManager>().WinnedLevel();

            mario.GetComponent<Animator>().SetTrigger("winLevel");
            mario.GetComponent<Movement.MarioMovement>().enabled = false;
            Invoke(nameof(ShowPanelGameOver), 2f);

            
        }

        public void EnableFinalGO()
        {
            finalGO.SetActive(true);
        }
    }

}
