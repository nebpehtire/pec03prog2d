using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace MSSUOC.Mario.Manager
{

    public class MenuManager : MonoBehaviour
    {
        /// <summary>
        /// Sale de la aplicación
        /// </summary>
        public void BTN_QuitGame()
        {
            FindObjectOfType<MusicManager>().PlayClick();
            Application.Quit();

        }

        /// <summary>
        /// Carga el nivel 1
        /// </summary>
        public void BTN_PlayGame()
        {
            FindObjectOfType<MusicManager>().PlayClick();
            SceneManager.LoadScene("Level2-PEC03");
        }
    }

}
