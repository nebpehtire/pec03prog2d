using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MSSUOC.Mario.Entities;


namespace MSSUOC.Mario.Movement
{

    /// <summary>
    /// Movimiento básico
    /// </summary>
    public class IAMovementGoomba : MonoBehaviour
    {
        public EnemyMovement enemyMovement;
        public GameObject target;

        private int index = 0;

        private void Awake()
        {
            enemyMovement = GetComponentInParent<EnemyMovement>();
        }


        // Update is called once per frame
        void Update()
        {
            index++;
            if (index%10 == 0)
            {
                if (target != null)
                {

                    float positionXcross = enemyMovement.transform.position.x - target.transform.position.x;
                    
                    //Debug.Log(positionXcross);
                    if (positionXcross == 0)
                    {

                    }
                    else if (positionXcross < 0)
                    {
                        enemyMovement.isWalkingLeft = false;
                    }
                    else
                    {
                        enemyMovement.isWalkingLeft = true;
                    }
                }
            }

        }

        /// <summary>
        /// Si entra Mario en el trigger lo añadimos como target
        /// </summary>
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "Player")
            {
                this.target = collision.gameObject;
            }
        }

        /// <summary>
        /// Si Mario sale del trigger lo quitamos como target
        /// </summary>

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.tag == "Player")
                this.target = null;
        }




    }


}
