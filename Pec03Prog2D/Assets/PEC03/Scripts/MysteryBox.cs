using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MSSUOC.Mario.Entities
{
    /// <summary>
    /// Los diferentes tipos de bloques misteriosos
    /// </summary>
    public enum MysteryBlockType
    {
        Coin,
        MultipleCoin,
        PowerUp
    }


    public class MysteryBox : MonoBehaviour
    {
        [SerializeField] private int coinsCount;
        
        public MysteryBlockType mysteryBlockType = MysteryBlockType.Coin;

        private bool isActive = true;
        private Animator animator;

        private void Awake()
        {

            animator = GetComponentInParent<Animator>();
            
            
            if (mysteryBlockType == MysteryBlockType.Coin && coinsCount == 0)
            {
                coinsCount = 1;
            }
            if (mysteryBlockType == MysteryBlockType.MultipleCoin)
            {
                coinsCount = 10;
            }
        }


        /// <summary>
        /// Si el jugador toca y está activo (no empty) ejecuta animación
        /// Si está empty aplica la booleana isEmpty del animator
        /// </summary>
        /// <param name="collision"></param>
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "Player" && isActive == true)
            {
                if (mysteryBlockType == MysteryBlockType.Coin)
                {

                    SumAndSound();

                    
                    animator.SetBool("isEmpty", true);

                    isActive = false;
                }
                else if (mysteryBlockType == MysteryBlockType.MultipleCoin)
                {

                    SumAndSound();
                    coinsCount--;
                    if (coinsCount == 0)
                    {
                        animator.SetBool("isEmpty", true);
                        isActive = false;
                    }

                }
                
                

            }
                
        }

        /// <summary>
        /// inicia animación, suma coin e inicia sonido
        /// </summary>
        private void SumAndSound()
        {
            animator.SetBool("marioTouchDownCoin", true);
            FindObjectOfType<Manager.GameManager>().SumAcoin();
            FindObjectOfType<Manager.MusicManager>().PlayOnShotSound(Manager.MarioSound.Coin);
        }
    }


}
