using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Bullet lanzado por Mario
/// </summary>
public class Bullet : MonoBehaviour
{

    public GameObject fxTouchFloor;
    public GameObject fxTouchEnemy;
    // Start is called before the first frame update

    /// <summary>
    /// Si toca el suelo desaparece, si toca un enemigo lo desplaza un poco y desaparece
    /// </summary>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Floor")
        {
            var newfx = Instantiate(fxTouchFloor, transform.position, transform.rotation);
            Destroy(gameObject);
        }
        else if (collision.transform.tag == "Enemy")
        {
            var newfx = Instantiate(fxTouchEnemy, collision.transform.position, transform.rotation);
            Destroy(gameObject);
        }
           
    }
}
