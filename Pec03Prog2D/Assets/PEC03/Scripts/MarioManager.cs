using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MSSUOC.Mario.Entities
{


    public enum MarioState
    {
        MiniMario,
        BigMArio,
        DeadMario
    }

    public class MarioManager : MonoBehaviour
    {

        public MarioState marioState;
        private Animator marioAnimator;
        //private float damageTimer;
        //private float timeToNewDamage;
        private bool receivedDamage;


        public GameObject fxDied;

        private void Awake()
        {
            marioState = MarioState.MiniMario;
            marioAnimator = GetComponent<Animator>();

            receivedDamage = false;
        }

        //private void Update()
        //{
        //    if (receivedDamage)
        //    {
        //        damageTimer -= Time.deltaTime;
        //        if (damageTimer <= 0)
        //        {
        //            receivedDamage = false;
        //        }
        //    }
        //}

        /// <summary>
        /// Mario muere
        /// Se desactivan los collider, movimiento
        ///
        /// Se ejecuta la música de vida perdida
        /// Se cambia al estado GameOver
        /// 
        /// </summary>
        private void IsDead()
        {
            GetComponent<BoxCollider2D>().enabled = false;
            GetComponent<CircleCollider2D>().enabled = false;
            GetComponent<Movement.MarioMovement>().enabled = false;
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * 125);

            FindObjectOfType<Manager.MusicManager>().StopMusic();
            FindObjectOfType<Manager.MusicManager>().PlayOnShotSound(Manager.MarioSound.LifeDown);
            FindObjectOfType<Manager.GameManager>().stateGame = Manager.StateGame.GameOver;
            marioState = MarioState.DeadMario;
            
            marioAnimator.SetBool("isDead", true);
            StartCoroutine(RespawnOrDied());

            GameObject fx = Instantiate(fxDied);
            fx.transform.position = this.transform.position;
            fx.transform.position -= new Vector3(0, 0, 2f);
        }

        /// <summary>
        /// Función para implementar la conversión de bigmario a mini mario, no realizada en el juego
        /// </summary>
        private void BigToMiniMario()
        {
            marioAnimator.SetBool("DamageBeforeDead", true);
            //damageTimer = timeToNewDamage;
            receivedDamage = true;
        }

        /// <summary>
        /// Función ejecutada cuando entramos en trigger de caida
        /// </summary>
        public void FallDie()
        {
            IsDead();
        }
        /// <summary>
        /// Mario recibe daño
        /// Si es big, cambiaria a mini
        /// Si es mini, vida perdida
        /// </summary>
        public void ReceiveDamage()
        {
            if (marioState == MarioState.MiniMario && !receivedDamage)
                this.IsDead();
            else if (marioState == MarioState.BigMArio && !receivedDamage)
                this.BigToMiniMario();
        }

        /// <summary>
        /// Temporizador para lanzar función de gamemanager y destruir este mario
        /// </summary>
        /// <returns></returns>
        IEnumerator RespawnOrDied()
        {
            yield return new WaitForSeconds(2.5f);
            FindObjectOfType<Manager.GameManager>().MarioHasLosedOneLife();
            Destroy(this.gameObject);
        }

        /// <summary>
        /// Función para lanzar gamemanager winned play
        /// </summary>
        public void Win()
        {
            FindObjectOfType<Manager.GameManager>().WinnedPlay();
        }


    }

}
