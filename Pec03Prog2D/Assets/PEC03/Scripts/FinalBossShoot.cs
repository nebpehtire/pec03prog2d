using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using MSSUOC.Mario.Movement;


namespace MSSUOC.Mario.Shoot
{

    /// <summary>
    /// Shoot del Final Boss
    /// </summary>
    public class FinalBossShoot : MonoBehaviour
    {
        private float timer = 0.0f;
        public GameObject bulletFinalBoss;
        private IAMovementGoomba iaMovement;
        private Vector3 plusPosition = new Vector3(0, 1f, 0);
        private Vector3 forceShoot = new Vector3(2, 1, 0);

        public float forceToShot = 0;

        private void Awake()
        {
            iaMovement = GetComponent<IAMovementGoomba>();
        }

        /// <summary>
        /// Si está el target activo y el timer > 2.5 lanza un nuevo bullet en dirección al Target
        /// </summary>
        void Update()
        {
            timer += Time.deltaTime;
            if (iaMovement.target == null)
                timer = 0f;
            else if (iaMovement.target != null && timer > 2.5f)
            {
                if (GetComponentInParent<EnemyMovement>().isWalkingLeft)
                {
                    plusPosition.x = -0.5f;
                    forceShoot.x = -2;
                }
                    
                else
                {
                    plusPosition.x = 0.5f;
                    forceShoot.x = 2;
                }
                    

                timer = 0;
                var bullet = Instantiate(bulletFinalBoss, transform.position + plusPosition, transform.rotation);
                bullet.GetComponent<Rigidbody2D>().velocity = forceShoot.normalized * forceToShot;
                GetComponent<Animator>().SetTrigger("bullet");
                FindObjectOfType<Manager.MusicManager>().PlayOnShotSound(Manager.MarioSound.FinalBossThrowBullet);
            }

        }
    }

}
