using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


namespace MSSUOC.Mario.Manager
{

    public class ScoreManager : MonoBehaviour
    {
        public TextMeshProUGUI score;
        public TextMeshProUGUI coins;
        public TextMeshProUGUI timeLeft;

        ///se crean los formatos a mostrar
        private string fmtScore = "000000";
        private string fmtCoins = "00";
        private string fmtTime = "000";



        /// <summary>
        /// Muestra los datos pasados por valor
        /// </summary>
        /// <param name="_score"></param>
        /// <param name="_coins"></param>
        /// <param name="_timeLeft"></param>
        public void AllScoresByValor(int _score, int _coins, int _timeLeft)
        {

            score.text = _score.ToString(fmtScore);
            coins.text = "x"+ _coins.ToString(fmtCoins);
            timeLeft.text = _timeLeft.ToString(fmtTime);

        }

        /// <summary>
        /// muestra el score pasado por valor
        /// </summary>
        /// <param name="_score"></param>
        public void ScoreByInt(int _score)
        {
            score.text = _score.ToString(fmtScore);
        }

        /// <summary>
        /// Muestra las monedas pasadas por valor
        /// </summary>
        /// <param name="_coins"></param>
        public void CoinsByInt(int _coins)
        {
            coins.text = "x" + _coins.ToString(fmtCoins);
        }

        /// <summary>
        /// Muestra el tiempo pasado por valor
        /// </summary>
        /// <param name="_timeLeft"></param>
        public void TimeByInt(int _timeLeft)
        {
            timeLeft.text = _timeLeft.ToString(fmtTime);
        }


   





    }

}
