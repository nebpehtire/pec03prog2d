using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using MSSUOC.Mario.Entities;

namespace MSSUOC.Mario.Movement
{


    public class FollowMario : MonoBehaviour
    {

        public MarioManager marioGO;
        public bool gameTower;
        public float speedFolowing = 5.0f;


        /// <summary>
        /// La cámara sigue a mario por el level
        /// /// </summary>
        void Update()
        {
            if (marioGO != null && marioGO.marioState != MarioState.DeadMario)
            {


                if (transform.position.x < marioGO.transform.position.x && !gameTower)
                {
                    Vector3 position = transform.position;
                    position.x = marioGO.transform.position.x;
                    transform.position = position;
                }
                else {
                    Vector3 position = transform.position;
                    position.x = marioGO.transform.position.x;
                    position.z = -5f;
                    transform.position = position;
                    
                  
                    if ((transform.position.y + 3) < marioGO.transform.position.y)
                    {
                        position.y = marioGO.transform.position.y + 3f;
                    } else if ((transform.position.y - 3) > marioGO.transform.position.y)
                    {
                        position.y = marioGO.transform.position.y + 3f;
                    }
                    transform.position = Vector3.MoveTowards(transform.position, position, speedFolowing * Time.deltaTime);
                }
            }

        }

    }
}