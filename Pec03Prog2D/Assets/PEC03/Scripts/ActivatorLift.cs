using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MSSUOC.Mario.Entities;


namespace MSSUOC.Mario.Movement
{

    /// <summary>
    /// Activa un lift dependiendo de si el enemigo está muerto
    /// </summary>
    

    public class ActivatorLift : MonoBehaviour
    {

        public Enemy enemyActivate;
        public Animator animatorLift;
        private bool activated = false;

        // Start is called before the first frame update
        void Start()
        {
            if (enemyActivate == null || animatorLift == null)
            {
                Debug.Log("Error on the Lift, destroyme");
                Destroy(this.gameObject);
            }
            
        }

        // Update is called once per frame
        void Update()
        {
            if (enemyActivate.isDeath == true && !activated)
            {
                animatorLift.SetBool("activateLift", true);
                activated = true;
            }
                
        }
    }

}
