using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MSSUOC.Mario.Manager;
using MSSUOC.Mario.Entities;

namespace MSSUOC.Mario.Movement
{



    public class MarioMovement : MonoBehaviour
    {

        private Rigidbody2D marioRigidbody2D;
        private Animator marioAnimator;
        private float horizontalMovement;
        public bool touchingGround;


        private Vector3 diference = new Vector3(0.35f, 0, 0);


        private Vector3 goingRight = new Vector3(1.0f, 1.0f, 1.0f);
        private Vector3 goingLeft = new Vector3(-1.0f, 1.0f, 1.0f);


        public float jumpForce;
        public float speed;
        private float speedRun;
        private float speedGuard;

        private Vector2 _right = new Vector2(1.0f, 0.0f);
        private MusicManager musicManager;

        private float timeDelayShot = 5.0f;
        private bool isGoingToRight = true;

        public GameObject bullet;



        // Start is called before the first frame update
        void Start()
        {

            musicManager = FindObjectOfType<MusicManager>();

            marioRigidbody2D = GetComponent<Rigidbody2D>();
            marioAnimator = GetComponent<Animator>();

            speedRun = 1.5f * speed;
            speedGuard = speed;
        }



        // Update is called once per frame
        void Update()
        {
            timeDelayShot += Time.deltaTime;

            touchingGround = TouchingGround();
            if (touchingGround)
            {
                marioAnimator.SetBool("jumping", false);
            }
            else
            {
                speed = speedGuard;
                CheckDownRays();
            }



            if ((Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space)) && touchingGround)
                Jump();

            if ((Input.GetKeyDown(KeyCode.LeftShift)))
                speed = speedRun;
            if ((Input.GetKeyUp(KeyCode.LeftShift)))
                speed = speedGuard;

            if (Input.GetKeyDown(KeyCode.Mouse0) && timeDelayShot > 1.0f)
                Shot();

        }


        private void Shot()
        {


            timeDelayShot = 0;

            var bulletInstance = Instantiate(bullet, transform.position, transform.rotation);

            if (isGoingToRight)
                bulletInstance.GetComponent<Rigidbody2D>().velocity = transform.right.normalized * 10.0f;
            else
                bulletInstance.GetComponent<Rigidbody2D>().velocity = transform.right.normalized * (-10.0f);

            musicManager.PlayOnShotSound(MarioSound.FireMario);
            marioAnimator.SetTrigger("shoot");
        }

        private void FixedUpdate()
        {
            horizontalMovement = Input.GetAxis("Horizontal");
            marioAnimator.SetBool("walking", horizontalMovement != 0.0f);
            if (horizontalMovement < 0.0f)
            {
                isGoingToRight = false;
                transform.localScale = goingLeft;
            }
            else if (horizontalMovement > 0.0f)
            {
                isGoingToRight = true;
                transform.localScale = goingRight;
            }



            marioRigidbody2D.position += horizontalMovement * _right * Time.deltaTime * speed;    


        }

        /// <summary>
        /// Salto de mario
        /// </summary>
        private void Jump()
        {
            marioAnimator.SetBool("jumping", true);
            marioRigidbody2D.AddForce(Vector2.up * jumpForce);
            musicManager.PlayOnShotSound(MarioSound.Jump);
        }


        /// <summary>
        /// Salto cuando golpeamos un enemigo
        /// </summary>
        private void JumpWithEnemyDestroy()
        {

            marioRigidbody2D.velocity = Vector3.zero;
            marioAnimator.SetBool("jumping", true);
            marioRigidbody2D.AddForce(Vector2.up * jumpForce);
            musicManager.PlayOnShotSound(MarioSound.TouchEnemy);
        }

        /// <summary>
        /// devuelve si estamos encima de un elemento
        /// Se añaden rayos delante y detrás para permitir el salto en el borde
        /// </summary>
        /// <returns></returns>
        private bool TouchingGround()
        {
            Vector3 rayPositionLeft = transform.position - diference;
            Vector3 rayPositionRight = transform.position + diference;
            //Vector3 rayPositionMiddle = transform.position;
            
            return Physics2D.Raycast(rayPositionLeft, Vector3.down, 0.5f) || Physics2D.Raycast(rayPositionRight, Vector3.down, 0.5f);  // devuelve true si delante y detrás del personaje toca el suelo
        }

        /// <summary>
        /// Chequea qué va a encontrar al caer
        /// Si es un enemigo acciona la función morir de este
        /// </summary>
        private void CheckDownRays()
        {
            Vector3 rayPositionLeft = transform.position - diference;
            Vector3 rayPositionMiddle = transform.position;
            Vector3 rayPositionRight = transform.position + diference;

            RaycastHit2D raycastHit2DLeft = Physics2D.Raycast(rayPositionLeft, Vector2.down, 1f);
            RaycastHit2D raycastHit2DMiddle = Physics2D.Raycast(rayPositionMiddle, Vector2.down, 1f);
            RaycastHit2D raycastHit2DRight = Physics2D.Raycast(rayPositionRight, Vector2.down, 1f);


            Debug.DrawRay(rayPositionMiddle, Vector3.down * 1f, Color.cyan);
            Debug.DrawRay(rayPositionLeft, Vector3.down * 1f, Color.cyan);
            Debug.DrawRay(rayPositionRight, Vector3.down * 1f, Color.cyan);

            bool touched = false;

            


            if (raycastHit2DLeft.collider != null && raycastHit2DLeft.collider.tag == "Enemy" && !raycastHit2DLeft.collider.GetComponent<Entities.Enemy>().isDeath && raycastHit2DLeft.collider.GetComponent<Enemy>().state != Enemy.EnemyState.withDamage)
            {
                if (raycastHit2DLeft.collider.GetComponent<Enemy>().MarioCauseDamage())
                    JumpWithEnemyDestroy();

                touched = true;
                
                
            }

            if (!touched && raycastHit2DMiddle.collider != null && raycastHit2DMiddle.collider.tag == "Enemy" && !raycastHit2DMiddle.collider.GetComponent<Enemy>().isDeath && raycastHit2DMiddle.collider.GetComponent<Enemy>().state != Enemy.EnemyState.withDamage)
            {
                if (raycastHit2DMiddle.collider.GetComponent<Enemy>().MarioCauseDamage())
                    JumpWithEnemyDestroy();

                touched = true;
                
                
            }
            if (!touched && raycastHit2DRight.collider != null && raycastHit2DRight.collider.tag == "Enemy" && !raycastHit2DRight.collider.GetComponent<Enemy>().isDeath && raycastHit2DRight.collider.GetComponent<Enemy>().state != Enemy.EnemyState.withDamage)
            {

                if (raycastHit2DMiddle.collider != null && raycastHit2DMiddle.collider.GetComponent<Enemy>().MarioCauseDamage())
                    JumpWithEnemyDestroy();


            }


        }



        /// <summary>
        /// Si el trigger es de bloques, anima estos
        /// Si el trigger es de caida, mario pierde una vida
        /// Si el trigger es de win, acaba la partida
        /// 
        /// </summary>
        /// <param name="collision"></param>

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "Blocks")
            {
                collision.GetComponent<Animator>().SetBool("miniMarioTouchDown", true);
            }

            if (collision.tag == "DieTriggers")
            {
                GetComponent<Entities.MarioManager>().FallDie();
            }

            if (collision.tag == "WinTrigger")
            {
                GetComponent<Entities.MarioManager>().Win();
                musicManager.StopMusic();
                musicManager.PlayOnShotSound(MarioSound.LevelComplete);
                this.enabled = false;
            }
        }

        /// <summary>
        /// Cuando sale de un trigger de respawn se graban los datos del respawn
        /// </summary>
        /// <param name="collision"></param>

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.tag == "Respawn")
            {
                Manager.GameManager gameManager = FindObjectOfType<Manager.GameManager>();
                gameManager.MariosHasEnteredInARespawn(collision.transform.position);
                collision.gameObject.SetActive(false);
                
            }

            
        }
    }

}
