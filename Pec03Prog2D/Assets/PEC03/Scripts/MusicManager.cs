using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MSSUOC.Mario.Manager
{
    public enum MarioSound
    {
        GroundTheme,
        GameOver,
        Jump,
        LifeUP,
        LifeDown,
        Tuberia,
        LevelComplete,
        TouchEnemy,
        Coin,
        FireMario,
        MarioWinLevel,
        FinalBossThrowBullet,
        FinalBossDead
    }

    public class MusicManager : MonoBehaviour
    {

        public AudioClip groundTheme;
        public AudioClip gameOver;
        public AudioClip jump;
        public AudioClip lifeUp;
        public AudioClip lifeDown;
        public AudioClip tuberia;
        public AudioClip levelComplete;
        public AudioClip touchEnemy;
        public AudioClip coin;

        public AudioClip fireMario;
        public AudioClip marioWinLevel;

        public AudioClip finalBossThrowBullet;
        public AudioClip finalBossDead;

        public AudioClip basicClick;

        private AudioSource audioSource;

        private static MusicManager musicManager;

        private Dictionary<MarioSound, AudioClip> audioDictionary;

        private void Awake()
        {
            DontDestroyOnLoad(this);

            if (musicManager == null)
            {
                musicManager = this;
            }
            else
            {
                Destroy(gameObject);
            }

            audioSource = GetComponent<AudioSource>();
        }


        /// <summary>
        /// Crea el diccionario que se usará para ejecutar los sonidos del juego
        /// </summary>
        private void CreateDictionary()
        {
            audioDictionary = new Dictionary<MarioSound, AudioClip>();

            audioDictionary.Add(MarioSound.GroundTheme, groundTheme);
            audioDictionary.Add(MarioSound.GameOver, gameOver);
            audioDictionary.Add(MarioSound.Jump, jump);
            audioDictionary.Add(MarioSound.LifeUP, lifeUp);
            audioDictionary.Add(MarioSound.LifeDown, lifeDown);
            audioDictionary.Add(MarioSound.Tuberia, tuberia);
            audioDictionary.Add(MarioSound.LevelComplete, levelComplete);
            audioDictionary.Add(MarioSound.TouchEnemy, touchEnemy);
            audioDictionary.Add(MarioSound.Coin, coin);
            audioDictionary.Add(MarioSound.FireMario, fireMario);
            audioDictionary.Add(MarioSound.MarioWinLevel, marioWinLevel);
            audioDictionary.Add(MarioSound.FinalBossThrowBullet, finalBossThrowBullet);
            audioDictionary.Add(MarioSound.FinalBossDead, finalBossDead);

            

        }
        // Start is called before the first frame update
        void Start()
        {
            CreateDictionary();
        }


        /// <summary>
        /// Para la música
        /// </summary>
        public void StopMusic()
        {
            audioSource.Stop();
        }

        /// <summary>
        /// Pausa la música
        /// </summary>
        public void PauseMusic()
        {
            audioSource.Pause();
        }

        /// <summary>
        /// Inicia la música desde la pausa
        /// </summary>
        public void PlayMusic()
        {
            audioSource.Play();
        }

        /// <summary>
        /// Inicia la música desde el inicio
        /// </summary>
        public void StartMusic()
        {
            audioSource.clip = groundTheme;
            audioSource.Play();
        }

        /// <summary>
        /// Inicia un sonido, deacuerdo a un audioclip
        /// </summary>
        /// <param name="sound"></param>
        public void PlayOnShotSound(MarioSound sound)
        {
            if (audioDictionary.TryGetValue(sound, out AudioClip audioClip))
                audioSource.PlayOneShot(audioClip);
        }


        /// <summary>
        /// Sonido de click básico
        /// </summary>
        public void PlayClick()
        {
            audioSource.PlayOneShot(basicClick);
        }

        /// <summary>
        /// Música para el final del level si se gana
        /// </summary>
        public void WinnedLevel()
        {
            audioSource.Stop();

            PlayOnShotSound(MarioSound.LevelComplete);
            PlayOnShotSound(MarioSound.MarioWinLevel);
        }
        
    }

}
